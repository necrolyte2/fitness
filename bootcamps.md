# Full Body
## 20 minute full body
https://youtu.be/D_d1qyHATb0

## 20 minute HIIT
https://youtu.be/-Z0UWWNMEZ4

## ~30 minute full-ish
https://www.youtube.com/watch?v=N3e8UXMdqOM

# Legs
## 7 minute legs
https://youtu.be/UKM_3T2-Huc

## 12 minute legs
https://youtu.be/Fu_oExrPX68

# Abs

## 5 minute Abs
https://youtu.be/EWFNeJDvtGE?t=38

# Shoulders

## 6 minutes
https://youtu.be/tKU64bd4gaw?t=21

# Chest

## Getting better at pushups(8m)
https://www.youtube.com/watch?v=MdsOS1sbZCY