# fitness

Just a readme for fitness workouts

## Legs and Chest

| Workout         | Repetitions | Sets |
| --------------- | ----------- | ---- |
| Inplace Lung    | 15          | 3    |
| [Hydrant Leg Lift](https://www.youtube.com/watch?v=inPhueDHJxQ)  | 15 | 3 |
| Table Top back leg lift | 15 | 3 |
| Bench | 12 | 3 |
| Jump Squat or Frog Jumps | 15 | 3 |
| Deadlift | 15 | 3 |
| Squats | 15 | 3 |
| Chest Fly | 12 | 3 |

## Tri & Shoulders

| Workout | Repetitions | Sets |
| --- | --- | --- |
| Overhead Tricep(single hand) | 10 | 3 |
| Front Raise | 10 | 3 |
| Side Shoulder Raise | 10 | 3 |
| Dips(slow down, fast up, rotate hands out, don't collapse shoulders) | 10 | 3 |
| Overhead Shoulder Raise | 10 | 3 |
| Skull Crusher(tuck arms in to sides, single hand use other arm to stable, arm 90 degrees perpindicular to body) | 10 | 3 |
| Shoulder Pivot | 10 | 3 |
| Diamond Pushup(diamond should hit you in sternum) | 10 | 3 |

## Backs & Bis

| Workout | Repetitions | Sets |
| --- | --- | --- |
| Bicep Curl | 12 | 3 |
| Low Rows | 12 | 3 |
| Pushup | 12 | 3 |
| Superman | 10 | 3 |
| Shoulder Shrug | 12 | 3 |
| Back Fly | 12 | 3 |

## Abs

45s + 30s rest, 30s + 15s rest, 15s no rest

| Workout         |
| --------------- |
| Bicycle |
| Leg lift between weights |
| Crunches or situps |
| Plank Tank |
| Side Plank Left |
| Side Plank Right |
| Superman |
| Toe Touch |
| V-tuck |
| Penguin |

